# Dotfiles

## Requirements (Arch Linux)

### Base

* `base`
* `base-devel`
* `btrfs-progs`
* `cryptsetup`
* `dosfstools`
* `efibootmgr`
* `grub`
* `lvm2`
* `sudo`

### Desktop

* `acpilight`
* `alsa-utils`
* `brightnessctl`
* `dmenu`
* `feh`
* `gxmessage`
* `i3-wm`
* `i3blocks`
	* `acpi`
* `i3status`
* `lximage-qt`
* `lxrandr`
* `picom`
* `pulseaudio-alsa`
* `redshift`
* `xclip`
* `xorg`
* `xorg-xinit`
* `xsecurelock`

### Internet

* `librewolf` (AUR)
* `networkmanager`
* `openresolv`

### Kernels

* `linux`
* `linux-firmware`
* `linux-lts`

### Programming

* `bash-language-server`
* `clang`
* `git`
* `go`
* `lua-language-server`
* `luajit`
* `mold`
* `neovim`
	* `cmake`
* `nodejs-emmet-ls` (AUR)
* `rustup`
* `tailwincss-language-server`
* `typst`
* `vscode-html-languageserver`
* `vscode-json-languageserver`

### Shell

* `starship`
* `vivid`
* `wezterm`
* `zsh-autosuggestions`
* `zsh-syntax-highlighting`
* `zsh`

### Themes

* `bibata-cursor-theme` (AUR)
* `kvantum`
* `kvantum-theme-arc` (AUR)
* `lxappearance`
* `lxappearance-gtk3`
* `materia-gtk-theme`
* `papirus-icon-theme`
* `qt5ct`
* `ttf-jetbrains-mono`
* `ttf-jetbrains-mono-nerd`
* `ttf-nerd-fonts-symbols-mono`
* `ttf-ubuntu-font-family`
* `vimix-gtk-themes-git` (AUR)

### Utils

* `bat`
* `cryfs`
* `cssmodules-language-server` (AUR)
* `dust`
* `exa`
* `fclones`
* `fd`
* `fzf`
* `git-delta`
* `gping`
* `man-db`
* `man-pages`
* `mkcert`
* `nnn`
* `ov` (AUR)
* `p7zip`
* `pacman-contrib`
* `powertop`
* `procs`
* `ripgrep`
* `rnr` (AUR)
* `rsync`
* `sad`
* `sc-im` (AUR)
	* `gnuplot`
* `tlp`
* `typescript-language-server`
* `typora-free` (AUR)
* `tokei`
* `usbutils`
* `wget`
* `zathura`
* `zoxide`
