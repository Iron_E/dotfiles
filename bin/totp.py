#!/usr/bin/env python3

# deps: zbar-tools, oathtool
#
# call it as toptp-token.py <qrimage-filename>
#
# 1. decodes the qrcode image via zbar-img
# 2. parses totp url and extracts parameters (secret, stepping, algo, ...)
# 3. calls oathtool to generate the current token
import subprocess
import sys

from urllib.parse import urlparse
from urllib.parse import parse_qs


if len(sys.argv) < 2:
    print("missing param")
    sys.exit(1)

url = subprocess.check_output(['zbarimg', '-q', '--nodbus', sys.argv[1]]).decode('utf8')

# print("URL: "+url)

parsed_url = urlparse(url)
parsed_qs = parse_qs(parsed_url.query)

if 'secret' not in parsed_qs:
    print("no secret")
    exit(1)

secret = parsed_qs['secret'][0]

cmd = cmd = ['oathtool', '-b', secret ]

if 'algorithm' in parsed_qs:
    cmd.append('--totp='+parsed_qs['algorithm'][0].strip())
else:
    cmd.append('--totp')

if 'digits' in parsed_qs:
    cmd.append('--digits='+parsed_qs['digits'][0].strip())

if 'period' in parsed_qs:
    cmd.append('--time-step-size='+parsed_qs['period'][0].strip())

# print(cmd)

code = subprocess.check_output(cmd).decode('utf8').strip()

print(code)
