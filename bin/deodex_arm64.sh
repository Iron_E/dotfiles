#!/usr/bin/env bash
path="framework/oat/arm64"

adb pull /system/framework ./framework

cp ./framework/services.jar ./services.jar-backup

vdexExtractor -i $path/services.vdex --ignore-crc-error

cdexExtractor -o classes.dex $path/services_classes.cdex

zip ./framework/services.jar ./classes.dex

zip -j ./framework/services.jar ./classes.dex

adb push ./framework/services.jar /system/framework

adb shell chmod 644 /system/framework/services.jar

adb shell chown root:root /system/framework/services.jar

rm -R ./classes.dex ./framework/
