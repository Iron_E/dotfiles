function fzw --wraps='word --cp (wd /mnt/vaults/words fzf)' --description 'alias fzw=word --cp (wd /mnt/vaults/words fzf)'
  word --cp (wd /mnt/vaults/words fzf) $argv
end
