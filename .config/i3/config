# This file has been auto-generated by i3-config-wizard(1).
# It will not be overwritten, so edit it as you like.
#
# Should you change your keyboard layout some time, delete
# this file and re-run i3-config-wizard(1).
#

# directory of the i3 configs
set $dir ~/.config/i3

# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod4
set $alt Mod1

# Font for window titles. Will also be used by the bar unless a different font is used in the bar {} block below.
font pango:OpenDyslexicNerdFont Regular 11

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec $Terminal

# kill focused window
bindsym $mod+q kill

# start dmenu (a program launcher)
bindsym $mod+d     exec switch-window.sh
bindsym $mod+x     exec dmenu_run
bindsym $mod+space exec i3-dmenu-desktop

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left  focus left
bindsym $mod+Down  focus down
bindsym $mod+Up    focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left  move left
bindsym $mod+Shift+Down  move down
bindsym $mod+Shift+Up    move up
bindsym $mod+Shift+Right move right

# split down the vertical axis
bindsym $mod+v split h

# split down the horizontal axis
bindsym $mod+Shift+s split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout toggle split
bindsym $mod+t       layout tabbed
bindsym $mod+w       layout stacking

# toggle tiling / floating
bindsym XF86LaunchA floating toggle
bindsym $mod+$alt+f floating toggle

# change focus between tiling / floating windows
bindsym $alt+Tab focus mode_toggle

# focus the parent container
bindsym $mod+p focus parent

# focus the child container
bindsym $mod+c focus child

# Workspaces
##  Define names for default workspaces for which we configure key bindings later on.
# // We use variables to avoid repeating the names in multiple places.
set $ws1 "1 | Browsers"
set $ws2 "2 | Misc"
set $ws3 "3 | Editors"
set $ws4 "4 | Background"
set $ws5 "5 | Comms"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

## switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

## move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

## Move focused workspace to another monitor
bindsym $mod+greater move workspace to output right
bindsym $mod+less move workspace to output left

## Assign applications to workspaces
### Browsers
assign [class="Chromium"] $ws1
assign [class="LibreWolf"] $ws1
assign [class="firefoxdeveloperedition"] $ws1
assign [class="dolphin"] $ws1

### Word Processors
assign [class="Apache NetBeans IDE Dev"] $ws3
assign [class="jetbrains-idea-ce"] $ws3
assign [instance="libreoffice"] $ws3
assign [title="ViNo"] $ws3

### Chat Applications
assign [class="Ripcord"] $ws5
assign [class="Riot"] $ws5
assign [class="Signal"] $ws5

# Restarting i3
## Reload the configuration file
bindsym $mod+Shift+c reload
## Restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
## Exit i3 (logs you out of your X session)
bindsym $mod+Shift+q exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# Resize window (you can also use the mouse for that)
mode "resize" {
	# These bindings trigger as soon as you enter the resize mode

	# Pressing left will shrink the window’s width.
	# Pressing right will grow the window’s width.
	# Pressing up will shrink the window’s height.
	# Pressing down will grow the window’s height.
	bindsym h resize shrink width  10 px or 10 ppt
	bindsym j resize grow   height 10 px or 10 ppt
	bindsym k resize shrink height 10 px or 10 ppt
	bindsym l resize grow   width  10 px or 10 ppt

	# same bindings, but for the arrow keys
	bindsym Left  resize shrink width  10 px or 10 ppt
	bindsym Down  resize grow   height 10 px or 10 ppt
	bindsym Up    resize shrink height 10 px or 10 ppt
	bindsym Right resize grow   width  10 px or 10 ppt

	# back to normal: Enter or Escape or $mod+r
	bindsym Return mode "default"
	bindsym Escape mode "default"
	bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# WINDOW APPEARANCE

## COLORS

### Variables

set $bg-color            "#af60af"
set $inactive-bg-color   "#35353a"
set $text-color          "#ffffff"
set $inactive-text-color "#c0c0c0"
set $urgent-bg-color     "#a80000"
set $indicator-color     "#22ff22"

### Assignment
#                       border              background         text                 indicator
client.focused          $bg-color           $bg-color          $text-color          $indicator-color
client.focused_inactive $inactive-bg-color  $inactive-bg-color $inactive-text-color $indicator-color
client.unfocused        $inactive-bg-color  $inactive-bg-color $inactive-text-color $indicator-color
client.urgent           $urgent-bg-color    $urgent-bg-color   $text-color          $indicator-color

## Properties
default_border pixel 1

#//hide_edge_borders both

# BAR
#// Start i3bar to display a workspace bar (plus the system information i3status finds out, if available)
bar {
	## SCRIPT
	font pango:OpenDyslexicNerdFont Regular 12
	status_command CONTRIB_DIR=$dir/i3blocks-contrib SCRIPT_DIR=$dir/scripts/i3blocks i3blocks -c ~/.config/i3/i3blocks.conf

	## POSITION
	position top

	## COLORS
	colors {
		background $inactive-bg-color
		separator "#757575"
		#//                  border             background         text
		focused_workspace    $bg-color          $bg-color          $text-color
		inactive_workspace   $inactive-bg-color $inactive-bg-color $inactive-text-color
		urgent_workspace     $urgent-bg-color   $urgent-bg-color   $text-color
	}
}

#=CUSTOM CONTENT=============================================#
## Variables
set $LightPopup gxmessage -buttons "" -center -nofocus -font "ubuntu" -ontop -sticky -timeout 1 -title "Screen Brightness" -wrap
set $Terminal wezterm cli spawn --new-window
set $updateVolumeInBar pkill -RTMIN+10 i3blocks
set $volumeSet amixer -q -D pulse sset Master

## Autorun--------------------------##
exec --no-startup-id i3-msg workspace $ws3
exec --no-startup-id i3-sensible-terminal
exec --no-startup-id picom --backend glx

### Wallpaper
exec_always --no-startup-id "feh --bg-fill ~/Pictures/wallpaper.jpg"

## Windows-----------------##
for_window [all] title_window_icon padding 4px
for_window [class="Gcolor3"]           floating enable
for_window [class="Gxmessage"]         floating enable
for_window [window_role="bubble"]      floating enable
for_window [window_role="pop-up"]      floating enable
for_window [window_role="Preferences"] floating enable
for_window [window_role="task_dialog"] floating enable
for_window [window_type="dialog"]      floating enable
for_window [window_type="menu"]        floating enable

##-Keybinds-------------------------##
### Calculator
bindsym XF86Calculator exec $Terminal python

### Lock computer
bindsym $mod+$alt+l exec "export XSECURELOCK_COMPOSITE_OBSCURER=0 && xsecurelock"

### Media Control
bindsym XF86AudioPlay  exec --no-startup-id playerctl play
bindsym XF86AudioPause exec --no-startup-id playerctl pause
bindsym XF86AudioNext  exec --no-startup-id layerctl next
bindsym XF86AudioPrev  exec --no-startup-id layerctl previous

### Screenshot
bindsym Print  exec lximage-qt --screenshot
bindsym $mod+XF86Eject  exec lximage-qt --screenshot

### Set temperature
bindsym $mod+Shift+XF86MonBrightnessDown exec --no-startup-id redshift -PO 1300
bindsym $mod+XF86MonBrightnessDown       exec --no-startup-id redshift -PO 2000
bindsym $mod+XF86MonBrightnessUp         exec --no-startup-id redshift -PO 3750
bindsym $mod+Shift+XF86MonBrightnessUp   exec --no-startup-id redshift -PO 5500

### Turn brightness up and down
# bindsym XF86MonBrightnessDown exec xbacklight -dec 3 && $Xpopup -title "Screen Brightness" $(xbacklight -get)
bindsym XF86MonBrightnessDown exec $LightPopup $(brightnessctl set 5%-)
bindsym XF86MonBrightnessUp   exec $LightPOpup $(brightnessctl set 5%+)

### Turn keyboard brightness up and down
bindsym XF86KbdBrightnessDown exec --no-startup-id brightnessctl -d "smc::kbd_backlight" set 10%-
bindsym XF86KbdBrightnessUp   exec --no-startup-id brightnessctl -d "smc::kbd_backlight" set 10%+

### Volume
bindsym XF86AudioRaiseVolume exec --no-startup-id $volumeSet 5%+    && $updateVolumeInBar
bindsym XF86AudioLowerVolume exec --no-startup-id $volumeSet 5%-    && $updateVolumeInBar
bindsym XF86AudioMute        exec --no-startup-id $volumeSet toggle && $updateVolumeInBar

###       Workspaces
bindsym   $mod+Tab       workspace next
bindsym   $mod+Shift+Tab workspace prev

### Yakuake
#bindsym XF86LaunchB exec lxterminal --drop-down
