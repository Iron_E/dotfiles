#!/usr/bin/env bash

# Vars
THEME_DIR="$XDG_CONFIG_HOME/Typora/themes"
REPOS_DIR="$THEME_DIR/_REPOS"

# Track themes
declare -a THEMES=(
	"aCluelessDanny/typora-theme-ursine"
	"adesurirey/typora-notes-dark-theme"
	"adrian-fuertes/typora-notion-theme"
	"Akash-Panigrahi/typora-saffron-theme"
	"AntonVanke/typora-Aspartate-theme"
	"AntonVanke/Typora-Cement-Theme"
	"AntonVanke/typora-mlike-theme"
	"BEATREE/typora-maize-theme"
	"belenos/typora-solarized"
	"BillChen2K/typora-theme-next"
	"blinkfox/typora-vue-theme"
	"ChristosBouronikos/typora-nord-theme"
	"ChristosBouronikos/typora-nord-theme/"
	"daodaolee/typora-scrolls"
	"elitistsnob/typora-cobalt-theme"
	"elitistsnob/typora-gitlab-theme"
	"etigerstudio/typora-misty-theme"
	"evgo2017/typora-theme-orange-heart"
	"FishionYu/typora-blubook-theme"
	"FishionYu/typora-inside-theme"
	"george-paul/typoratheme-paradox"
	"gilbertohasnofb/typora-panda-theme"
	"heisenburger/typora-theme-refine"
	"HelloRusk/Typora-Onigiri"
	"Henning16/typora-gitbook-theme"
	"HereIsLz/Fluent-Typora"
	"hliu202/typora-purple-theme"
	"IagoLast/paper"
	"jamstatic/Typora-theme"
	"JARVIS-AI/Typora-Lavender-Theme/"
	"kevinzhao2233/typora-theme-pie"
	"kinoute/typora-github-night-theme"
	"leaf-hsiao/catfish"
	"LeonWong0609/Typora-D42ker-Github-theme"
	"lfkdsk/techo.css"
	"li3zhen1/Fluent-Typora"
	"liangjingkanji/DrakeTyporaTheme"
	"lloyd094/Typora-Law-School"
	"lostkeys/Typora-Lostkeys-Theme"
	"LSTM-Kirigaya/typora-haru-theme"
	"MarMomento/typora-mo-theme"
	"muggledy/typora-dyzj-theme"
	"pomopopo/typora-theme-softgreen"
	"qqdaiyu55/engwrite-theme"
	"Soanguy/typora-theme-autumnus"
	"sonnie-sonnig/ia_typora/"
	"sweatran/typora-onedark-theme"
	"Teyler7/dracula-typora-theme"
	"tristone13th/typora-vintage-theme"
	"troennes/quartz-theme-typora"
	"typora/typora-ash-theme"
	"typora/typora-monospace-theme"
	"valfur03/Monokai-Theme-for-Typora"
	"wangjs-jacky/Turing-CSS"
	"xypnox/xydark-typora"
	"Y1chenYao/typora-mint-theme"
)

mkdir $REPOS_DIR
cd $REPOS_DIR

# Perform the update
for repo in "${THEMES[@]}"; do
	git clone "https://github.com/$repo"

	FOLDER=$(echo $repo | cut -d '/' -s -f 2)

	rsync -av --copy-links $FOLDER/* $THEME_DIR
	rm -rf $FOLDER
done

rsync -av --copy-links $THEME_DIR/theme/* $THEME_DIR
rsync -av --copy-links $THEME_DIR/src/* $THEME_DIR

rm -rf $THEME_DIR/theme $THEME_DIR/Tests $THEME_DIR/src

# Clean up
declare -a DEAD_FILETYPES=("html" "jpeg" "jpg" "md" "pdf" "png" "zip")
for ft in "${DEAD_FILETYPES[@]}"; do
	fd . $THEME_DIR -e $ft -X rm "{}"
done

# Remove empty dirs
fd . $THEME_DIR -t d -t e -X rm -r "{}"
