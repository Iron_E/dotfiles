#!/usr/bin/env bash

# Vars
DOTFILE_REPO=$(pwd)
DOTFILE_REPO_CONFIG="$DOTFILE_REPO/.config"
DOTFILE_REPO_ETC="$DOTFILE_REPO/etc"

rm -rf $DOTFILE_REPO_CONFIG

# Add new files
declare -A dotfiles=(
	# bat
	["$XDG_CONFIG_HOME/bat"]="$DOTFILE_REPO_CONFIG"
	# bin (scripts)
	["$HOME/bin/clinvoice"]="$DOTFILE_REPO/bin"
	["$HOME/bin/dartfmt_dir.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/debug_omnisharp.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/deodex.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/deodex_arm64.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/get_volume.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/git-fix.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/mkgif"]="$DOTFILE_REPO/bin"
	["$HOME/bin/mkhosts.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/mkpseudo.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/mkpseudo_list.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/pandocx"]="$DOTFILE_REPO/bin"
	["$HOME/bin/pip-update.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/pub-publish.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/sc-im-help.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/START.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/START-EXTRA.sh"]="$DOTFILE_REPO/bin"
	["$HOME/bin/totp.py"]="$DOTFILE_REPO/bin"
	["$HOME/bin/word"]="$DOTFILE_REPO/bin"
	["$HOME/bin/wordcp"]="$DOTFILE_REPO/bin"
	["$HOME/bin/xmessage_light_info.sh"]="$DOTFILE_REPO/bin"
	# Cargo
	["$HOME/.cargo"]="$DOTFILE_REPO"
	# Editorconfig
	["$HOME/.editorconfig"]="$DOTFILE_REPO"
	# Environment
	["/etc/environment"]="$DOTFILE_REPO_ETC"
	["/etc/NetworkManager/NetworkManager.conf"]="$DOTFILE_REPO_ETC/NetworkManager"
	["/etc/resolvconf.conf"]="$DOTFILE_REPO_ETC"
	["/etc/X11/xorg.conf.d/00-keyboard.conf"]="$DOTFILE_REPO_ETC/X11/xorg.conf.d"
	["/etc/X11/xorg.conf.d/40-libinput.conf"]="$DOTFILE_REPO_ETC/X11/xorg.conf.d"
	# Fish
	["$XDG_CONFIG_HOME/fish"]="$DOTFILE_REPO_CONFIG"
	# Fontconfig
	["$XDG_CONFIG_HOME/fontconfig"]="$DOTFILE_REPO_CONFIG"
	# Git
	["$XDG_CONFIG_HOME/git-cliff"]="$DOTFILE_REPO_CONFIG/git-cliff"
	["$XDG_CONFIG_HOME/git/config"]="$DOTFILE_REPO_CONFIG/git"
	["$XDG_CONFIG_HOME/git/message"]="$DOTFILE_REPO_CONFIG/git"
	# Gtk
	["$XDG_CONFIG_HOME/gtk-2.0/gtkrc"]="$DOTFILE_REPO_CONFIG/gtk-2.0"
	["$XDG_CONFIG_HOME/gtk-3.0/settings.ini"]="$DOTFILE_REPO_CONFIG/gtk-3.0"
	["$XDG_CONFIG_HOME/gtk-4.0/settings.ini"]="$DOTFILE_REPO_CONFIG/gtk-4.0"
	# i3
	["$XDG_CONFIG_HOME/i3/config"]="$DOTFILE_REPO_CONFIG/i3"
	["$XDG_CONFIG_HOME/i3/i3blocks.conf"]="$DOTFILE_REPO_CONFIG/i3"
	["$XDG_CONFIG_HOME/i3/scripts"]="$DOTFILE_REPO_CONFIG/i3"
	# Kvantum
	["$XDG_CONFIG_HOME/Kvantum"]="$DOTFILE_REPO_CONFIG"
	# LSD
	["$XDG_CONFIG_HOME/lsd"]="$DOTFILE_REPO_CONFIG"
	# Lxterminal
	["$XDG_CONFIG_HOME/lxterminal"]="$DOTFILE_REPO_CONFIG"
	# Maven
	["$XDG_CONFIG_HOME/maven"]="$DOTFILE_REPO_CONFIG"
	# Picom
	["$XDG_CONFIG_HOME/picom.conf"]="$DOTFILE_REPO_CONFIG"
	# SC-IM
	["$XDG_CONFIG_HOME/scimrc"]="$DOTFILE_REPO_CONFIG"
	# Starship
	["$XDG_CONFIG_HOME/starship.toml"]="$DOTFILE_REPO_CONFIG"
	# Typora
	["$XDG_CONFIG_HOME/Typora/themes/_REPOS/update.sh"]="$DOTFILE_REPO_CONFIG/Typora/themes/_REPOS"
	# Update Scripts
	["$HOME/Repos/update.sh"]="$DOTFILE_REPO/Repos"
	# Vim
	["$XDG_CONFIG_HOME/nvim/after"]="$DOTFILE_REPO_CONFIG/nvim"
	["$XDG_CONFIG_HOME/nvim/colors"]="$DOTFILE_REPO_CONFIG/nvim"
	["$XDG_CONFIG_HOME/nvim/ftdetect"]="$DOTFILE_REPO_CONFIG/nvim"
	["$XDG_CONFIG_HOME/nvim/ftplugin"]="$DOTFILE_REPO_CONFIG/nvim"
	["$XDG_CONFIG_HOME/nvim/init"]="$DOTFILE_REPO_CONFIG/nvim"
	["$XDG_CONFIG_HOME/nvim/lua"]="$DOTFILE_REPO_CONFIG/nvim"
	["$XDG_CONFIG_HOME/nvim/syntax"]="$DOTFILE_REPO_CONFIG/nvim"
	["$XDG_CONFIG_HOME/nvim/eclipse-formatter.xml"]="$DOTFILE_REPO_CONFIG/nvim"
	["$XDG_CONFIG_HOME/nvim/init.lua"]="$DOTFILE_REPO_CONFIG/nvim"
	# Wezterm
	["$XDG_CONFIG_HOME/wezterm"]="$DOTFILE_REPO_CONFIG"
	# X
	["$HOME/.xinitrc"]="$DOTFILE_REPO"
	["$HOME/.Xmodmap"]="$DOTFILE_REPO"
	["$HOME/.Xmodmap-alienware"]="$DOTFILE_REPO"
	["$HOME/.Xmodmap-external-keyboard"]="$DOTFILE_REPO"
	["$HOME/.Xmodmap-keyboard"]="$DOTFILE_REPO"
	["$HOME/.Xresources"]="$DOTFILE_REPO"
	# Zathura
	["$XDG_CONFIG_HOME/zathura/zathurarc"]="$DOTFILE_REPO_CONFIG/zathura"
)

# Perform the update
for srcfile in "${!dotfiles[@]}"; do
	# Get the destination path.
	dest="${dotfiles[$srcfile]}"

	# Determine what directory must exist in order for `rsync` to succeed.
	if [[ "$dest" != "$DOTFILE_REPO" ]]; then
		mkdir -p "$dest"
	fi

	# Perform the rsync.
	rsync -av --copy-links "$srcfile" "${dotfiles[$srcfile]}"
done

# Cleanup
find "$DOTFILE_REPO_CONFIG/i3" -name ".git*" -print0 | xargs -0 rm -rf
rm -f "$DOTFILE_REPO_CONFIG/nvim/lua/.luarc.json"
#bat .zshrc | head --lines=-2 | bat > .zshrc2
#mv .zshrc2 .zshrc
