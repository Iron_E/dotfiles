source "$HOME/.profile"
export HISTFILE="$XDG_STATE_HOME/zsh/history"

# Initialize completion
autoload -U compaudit compinit
autoload -U +X bashcompinit && bashcompinit
compinit -u -C -d "${ZSH_COMPDUMP}"
zmodload -i zsh/complist

unsetopt menu_complete   # do not autoselect the first completion entry
unsetopt flowcontrol
setopt auto_menu         # show completion menu on successive tab press
setopt complete_in_word
setopt always_to_end

expand-or-complete-with-dots() {
	 # use $COMPLETION_WAITING_DOTS either as toggle or as the sequence to show
	 [[ $COMPLETION_WAITING_DOTS = true ]] && COMPLETION_WAITING_DOTS="%F{red}…%f"
	 # turn off line wrapping and print prompt-expanded "dot" sequence
	 printf '\e[?7l%s\e[?7h' "${(%)COMPLETION_WAITING_DOTS}"
	 zle expand-or-complete
	 zle redisplay
}

zle -N expand-or-complete-with-dots
bindkey -M menuselect '^o' accept-and-infer-next-history
bindkey -M vicmd "^I" expand-or-complete-with-dots
bindkey -M viins "^I" expand-or-complete-with-dots

zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' list-colors ''
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
zstyle ':completion:*:*:*:*:processes' command "ps -u $USERNAME -o pid,user,comm -w -w"
zstyle ':completion:*:cd:*' tag-order local-directories directory-stack path-directories
zstyle ':completion:*' use-cache yes
zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh
zstyle '*' single-ignored show
zstyle ':completion:*:*:*:users' ignored-patterns \
        adm amanda apache at avahi avahi-autoipd beaglidx bin cacti canna \
        clamav daemon dbus distcache dnsmasq dovecot fax ftp games gdm \
        gkrellmd gopher hacluster haldaemon halt hsqldb ident junkbust kdm \
        ldap lp mail mailman mailnull man messagebus  mldonkey mysql nagios \
        named netdump news nfsnobody nobody nscd ntp nut nx obsrun openvpn \
        operator pcap polkitd postfix postgres privoxy pulse pvm quagga radvd \
        rpc rpcuser rpm rtkit scard shutdown squid sshd statd svn sync tftp \
        usbmux uucp vcsa wwwrun xfs '_*'

# Initialize colors
setopt auto_cd
setopt multios
setopt prompt_subst

zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"


# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
	export EDITOR='vim'
fi

# Use (Neo)vim keybindings in the shell
set -o vi

# Aliases

alias cat='bat --style=plain'
alias clr='clear && clear'
alias fzw='(z words && wordcp $(fzf))'
alias mkdir='mkdir -p'
alias mvn="mvn -gs '$XDG_CONFIG_HOME/maven/settings.xml'"
alias n='nnn'
alias nc='nmcli -p'
alias nukeuml='java -DPLANTUML_LIMIT_SIZE=8192 -Xmx1024m -jar /usr/share/java//plantuml/plantuml.jar "$@"'
alias wget="wget --hsts-file='$XDG_CACHE_HOME/wget-hsts'"
alias ws='aiksaurus'
alias ytdl='youtube-dl'

## git
alias g='git'
alias ga='g add'
alias gaa='ga -A'
alias gap='ga --patch'
alias gb='g rebase'
alias gbi='gb -i'
alias gbia='gbi --autosquash'
alias gc='g cherry-pick'
alias gc='g commit'
alias gca='gc --amend'
alias gcb='g absorb'
alias gcbr='g absorb --and-rebase'
alias gd='g diff'
alias gdh='gd HEAD'
alias gds='gd --stat'
alias gdsh='gds HEAD'
alias ge='g restore'
alias gf='g fetch'
alias gfa='gf --all'
alias gh='g show'
alias git-graph='git log --graph --abbrev-commit --decorate --format=format:"%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)" --all'
alias git-sign='git rebase --exec "git commit --amend --no-edit -n -S" -i'
alias gl='g log'
alias gp='g push'
alias gpl='g pull'
alias gpt='gp --tags'
alias gr='g reset'
alias gs='g status'
alias gt='g stash'
alias gw='g switch'
alias gwc='g switch -c'
alias gwd='g switch -d'

## eza
alias l='ls -l --git --git-repos --header'
alias la='l -a'
alias ls='eza --group-directories-first --icons'

## nvim
alias nim='env TERM=wezterm nim'
alias nvim='env TERM=wezterm nvim'

## xvlip
alias P='xclip -o -selection clipboard'
alias Y='xclip -i -selection clipboard'

## Setup
is_installed() {
	return $(test -x "$(which "$1")")
}

source_if_exists() {
	[[ -f "$1" ]] && source "$1"
}

is_installed dagger && eval "$(dagger completion zsh)"
is_installed wezterm && eval "$(wezterm shell-completion --shell zsh)"
is_installed zoxide && eval "$(zoxide init zsh)"

is_installed fzf &&
	source_if_exists /usr/share/fzf/completion.zsh &&
	source_if_exists /usr/share/fzf/key-bindings.zsh

is_installed starship &&
	eval "$(starship init zsh)" &&
	eval "$(starship completions zsh)"

if [[ -f /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh ]]; then
	source_if_exists /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
	bindkey '^ ' autosuggest-accept
	ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#808080"
fi

source_if_exists /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
