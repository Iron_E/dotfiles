#!/usr/bin/env bash

PREV=$(pwd)
ROOT=$HOME/Repos

# go to the repo root
cd $ROOT

# update all repos
for d in $(fd -t d -d 1); do
	cd $d
	git pull
	cd $ROOT
done

#######################################
#                        _            #
#  _ __   ___  _____   _(_)_ __ ___   #
# | '_ \ / _ \/ _ \ \ / / | '_ ` _ \  #
# | | | |  __/ (_) \ V /| | | | | | | #
# |_| |_|\___|\___/ \_/ |_|_| |_| |_| #
#######################################

NVIM_ROOT="$ROOT/neovim"
NVIM_BUILD="$NVIM_ROOT/build"
NVIM_SRC="$NVIM_ROOT/src"

cd $NVIM_ROOT

# Extract the source and if there were no errors, remove the previous build.
7z x "$NVIM_SRC.7z"
cd $NVIM_SRC
git pull &&
	make CMAKE_BUILD_TYPE=Release CMAKE_INSTALL_PREFIX=$NVIM_BUILD &&
	rm -rf $NVIM_BUILD &&
	make install &&
	ln -sf "$NVIM_BUILD/bin/nvim" "$HOME/bin/nim"
git reset --hard
git clean -fxd
cd $NVIM_ROOT &&
	rm "$NVIM_SRC.7z" &&
	7z a "$NVIM_SRC.7z" $NVIM_SRC &&
	rm -rf $NVIM_SRC

################################
#                _             #
#  ___  ___     (_)_ __ ___    #
# / __|/ __|____| | '_ ` _ \   #
# \__ \ (_|_____| | | | | | |  #
# |___/\___|    |_|_| |_| |_|  #
################################
cd $ROOT/sc-im

make -C src
sudo make -C src install

# Return to whence we came
cd $PREV
